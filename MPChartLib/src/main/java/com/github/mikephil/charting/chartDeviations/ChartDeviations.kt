package com.github.mikephil.charting.chartDeviations

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.R
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.*
import com.github.mikephil.charting.components.LimitLine.LimitLabelPosition
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import java.text.SimpleDateFormat
import java.util.*


class ChartDeviations @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {


    ///drawXAxisValue 558 in Util

    private lateinit var chart: LineChart
    private var scaleLength = 0

    private var values: ArrayList<Entry> = ArrayList()

    private lateinit var tfLight: Typeface


    init {

//        for (i in 0 until 10) {
//            val horizonVal: Float = i.toFloat() + 0.2F
//            val verticalVal: Float = (Math.random() * (5 + 1)).toFloat()
//            values.add(Entry(horizonVal, verticalVal))
//        }


        values.add(Entry(0F, 0f))
        values.add(Entry(1F, 11f))
//        values.add(Entry(2F, 0f))


        initView()


        var valuesTMP = values

        try {
            for (item in 0..values.size) {
                if (values[item].y < 0 && values[item + 1].y > 0) {
                    var x = ((values[item + 1].x - values[item].x) / 2) + values[item].x
//                    var y = ((values[item].y - values[item].y))
                    valuesTMP.add(item + 1, Entry(x, 0F))
                } else if (values[item].y > 0 && values[item + 1].y < 0) {
                    var x = ((values[item + 1].x - values[item].x) / 2) + values[item].x
//                    var y = ((values[item].y - values[item].y))
                    valuesTMP.add(item + 1, Entry(x, -0.00001F))
                } else if (values[item].y == 0F && values[item + 1].y < 0) {
                    var x = ((values[item + 1].x - values[item].x) / 2) + values[item].x
//                    var y = ((values[item].y - values[item].y))
                    valuesTMP[item].y = -0.00001F
                }
            }
            values = valuesTMP
        } catch (e: Exception) {


        }


        setData(values)
    }


    private fun initView() {
        inflate(context, R.layout.chart_resonance, this)

        chart = findViewById(R.id.chart1)
        tfLight = Typeface.DEFAULT


        chart.setViewPortOffsets(10f, 0f, 10f, 70f)
        chart.setBackgroundColor(Color.TRANSPARENT)
        // no description text
        chart.description.isEnabled = false
        chart.legend.isEnabled = false
        // enable scaling and dragging
        chart.isDragEnabled = false
        chart.setScaleEnabled(false)
        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false)
        // enable touch gestures
        chart.setTouchEnabled(true)
        chart.setDrawGridBackground(false)


        // create marker to display box when values are selected
        val mv: MyMarkerView = MyMarkerView(context, R.layout.custom_marker_view)
        // Set the marker to the chart
        mv.chartView = chart
        chart.marker = mv

        chart.getAxisLeft().setDrawGridLines(false)
        chart.getXAxis().setDrawGridLines(false)
        chart.axisLeft.isEnabled = false
        chart.axisRight.isEnabled = false

        chart.axisLeft.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)

        chart.xAxis.isEnabled = false
//        chart.xAxis.position = XAxis.XAxisPosition.BOTTOM
        chart.xAxis.setDrawAxisLine(false)



        val ll2 = LimitLine(0f, "Lower Limit")
        ll2.lineWidth = 4f

//            ll2.enableDashedLine(10f, 10f, 0f);
        ll2.labelPosition = LimitLabelPosition.RIGHT_BOTTOM
        ll2.textSize = 10f
        ll2.typeface = tfLight


        var yAxis = chart.axisLeft
        var xAxis = chart.xAxis
        xAxis = chart.xAxis
        xAxis.axisMinimum = values.first().x
        xAxis.axisMaximum = values.last().x // because there are 250 data points
//
//        xAxis.setLabelCount(10) // if you want to display 0, 5 and 10s which are 3 values then put 3 else whatever of your choice.

//        xAxis.valueFormatter = MyXAxisFormatter()
        chart.xAxis.labelCount = values.count()





        // draw limit lines behind data instead of on top

        // draw limit lines behind data instead of on top
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)

        // add limit lines

        // add limit lines
//        yAxis.addLimitLine(ll1)
//        yAxis.addLimitLine(ll2)

        Log.e("!!!ax", "!!!!!!!!!")


        // don't forget to refresh the drawing
        chart.invalidate()


//        ??
//        chart.animateXY(2000, 2000)

    }


    fun setData(values: ArrayList<Entry>) {

        if (values[0].y != 0F) {
            values.add(0, Entry(0F, 0F))
        }
        if (values.last().y != 0F) {
            values.add(Entry(values.last().x + 1F, 0F))
        }




        for (i in values) {
            val xVal: Float = i.x
            if (scaleLength < xVal) {
                scaleLength = xVal.toInt()
            }
        }


        val set1: LineDataSet
        if (chart.data != null &&
            chart.data.dataSetCount > 0
        ) {
            set1 = chart.data.getDataSetByIndex(0) as LineDataSet
            set1.values = values

            chart.data.notifyDataChanged()
            chart.notifyDataSetChanged()
        } else {


            set1 = LineDataSet(values, "DataSet 1")
            set1.mode = LineDataSet.Mode.CUBIC_BEZIER
            set1.cubicIntensity = 0.2f
            set1.setDrawFilled(true)


            set1.setDrawCircles(true)





            set1.circleRadius = 5f
            set1.setCircleColor(Color.parseColor("#089379"))
//            set1.highLightColor = Color.rgb(244, 117, 117)

//            var grad : GradientColor = GradientColor(R.color.black, R.color.purple_700)
//            var listColor : List<GradientColor> = listOf(grad)
//            set1.gradientColors =  listColor
            set1.color = Color.parseColor("#089379")

//            set1.fillColor = Color.RED
            set1.fillAlpha = 10

            // drawables only supported on api level 18 and above
            val drawable =
                ContextCompat.getDrawable(context, R.drawable.bg_chart_deviations_positive)
            set1.fillDrawable = drawable


            set1.highLightColor = Color.YELLOW

            set1.setDrawHorizontalHighlightIndicator(false)
            set1.fillFormatter =
                IFillFormatter { dataSet, dataProvider -> chart.getAxisLeft().getAxisMinimum() }
            set1.lineWidth = 2F

            // create a data object with the data sets
            val data = LineData(set1)


            data.setValueTypeface(tfLight)


            data.setValueTextSize(9f)


            //value of value
            data.setDrawValues(false)

            set1.drawableGradientPositive = ContextCompat.getDrawable(context, R.drawable.bg_chart_deviations_positive)
            set1.drawableGradientNegative = ( ContextCompat.getDrawable(context, R.drawable.bg_chart_deviations_negative))


            // set data
            chart.renderer
            chart.data = data

            var xMax: Int = 0

            for (i in values) {
                if (i.x > xMax) {
                    xMax = i.x.toInt()
                }
            }















//


        }
    }




}


class MyXAxisFormatter : IAxisValueFormatter {
    private val days = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 12, 11)

//    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
//        if (days.getOrNull(value.toInt())!! < 5) {
//            axis?.textColor = (Color.RED)
//        } else if (days.getOrNull(value.toInt())!! > 5) {
//            axis?.textColor = (Color.BLUE)
//        }
//
//        axis?.mEntries
//        for (i in axis?.mEntries!!) {
////            Log.e("!!!i.toString()", i.toString())
////            Log.e("!!!ax", i.)
//        }
//
////        String().fo
//
////        Log.e("!!!ax", axis?.mEntries.toString())
//        return days.getOrNull(value.toInt()).toString() ?: value.toString()
////        return days.getOrNull(value.toInt()) ?: value.toString()
//    }


    override fun getFormattedValue(value: Float, axis: AxisBase?): String {

        return value.toString() ?: value.toString()

    }


}

class MyMarkerView(context: Context?, layoutResource: Int) :
    MarkerView(context, layoutResource) {
    private val tvContent: TextView = findViewById<View>(R.id.tvContent) as TextView

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    override fun refreshContent(
        e: Entry,
        highlight: Highlight
    ) {

//            tvContent.text = "" + Utils.formatNumber(
//                e.x,
//                0,
//                true
//            )

        tvContent.text = e.label +" -  ₽ " + String.format("%.3f", (e.y.toDouble() / 6000))

        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
    }

}