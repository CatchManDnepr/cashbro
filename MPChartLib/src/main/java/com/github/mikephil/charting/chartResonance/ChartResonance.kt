package com.github.mikephil.charting.chartResonance

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Typeface
import android.util.AttributeSet
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.TableLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.R
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.components.XAxis.XAxisPosition
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.MPPointF
import java.util.*


class ChartResonance @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {


    ///drawXAxisValue 558 in Util

    private lateinit var chart: LineChart
    private var scaleLength = 0

    private var values: ArrayList<Entry> = ArrayList<Entry>()

    private lateinit var tfLight: Typeface


    init {
        initView()


        for (i in 0 until 10) {
            val horizonVal: Float = i.toFloat() + 0.2F
            val verticalVal: Float = (Math.random() * (5 + 1)).toFloat()
            values.add(Entry(horizonVal, verticalVal))
        }
        setData(values)
    }


    private fun initView() {
        inflate(context, R.layout.chart_resonance, this)

        chart = findViewById(R.id.chart1)
        tfLight = Typeface.DEFAULT


        chart.setViewPortOffsets(10f, 0f, 10f, 70f)
        chart.setBackgroundColor(Color.TRANSPARENT)
        // no description text
        chart.description.isEnabled = false
        chart.legend.isEnabled = false
        // enable scaling and dragging
        chart.isDragEnabled = false
        chart.setScaleEnabled(false)
        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(false)
        // enable touch gestures
        chart.setTouchEnabled(true)
        chart.setDrawGridBackground(false)


        // create marker to display box when values are selected
        val mv: MyMarkerView = MyMarkerView(context, R.layout.custom_marker_view)
        // Set the marker to the chart
        mv.setChartView(chart)
        chart.marker = mv

        chart.getAxisLeft().setDrawGridLines(false)
        chart.getXAxis().setDrawGridLines(false)
        chart.axisLeft.isEnabled = false
        chart.axisRight.isEnabled = false

        chart.xAxis.position = XAxisPosition.BOTTOM
        chart.xAxis.setDrawAxisLine(false)


        var xAxis = chart.xAxis
        xAxis = chart.xAxis
//        xAxis.setAxisMinimum(0F)
//        xAxis.setAxisMaximum(250F) // because there are 250 data points
//
//        xAxis.setLabelCount(10) // if you want to display 0, 5 and 10s which are 3 values then put 3 else whatever of your choice.

//        xAxis.valueFormatter = MyXAxisFormatter()
        chart.xAxis.setLabelCount(20)

        // don't forget to refresh the drawing
        chart.invalidate()


//        ??
//        chart.animateXY(2000, 2000)

    }


    fun setData(values: ArrayList<Entry>) {

        if (values[0].y != 0F) {
            values.add(0, Entry(0F, 0F))
        }
        if (values.last().y != 0F) {
            values.add(Entry(values.last().x + 1F, 0F))
        }




        for (i in values) {
            val xVal: Float = i.x
            if (scaleLength < xVal) {
                scaleLength = xVal.toInt()
            }
        }


        val set1: LineDataSet
        if (chart.data != null &&
            chart.data.dataSetCount > 0
        ) {
            set1 = chart.data.getDataSetByIndex(0) as LineDataSet
            set1.values = values
            chart.data.notifyDataChanged()
            chart.notifyDataSetChanged()
        } else {


            set1 = LineDataSet(values, "DataSet 1")
            set1.mode = LineDataSet.Mode.CUBIC_BEZIER
            set1.cubicIntensity = 0.2f
            set1.setDrawFilled(true)


            set1.setDrawCircles(false)


            set1.lineWidth = 1.8f
            set1.circleRadius = 4f
            set1.setCircleColor(Color.WHITE)
            set1.highLightColor = Color.rgb(244, 117, 117)

//            var grad : GradientColor = GradientColor(R.color.black, R.color.purple_700)
//            var listColor : List<GradientColor> = listOf(grad)
//            set1.gradientColors =  listColor
//            set1.color = Color.WHITE
//            set1.fillColor = Color.WHITE
            set1.fillAlpha = 100

            // drawables only supported on api level 18 and above
            val drawable = ContextCompat.getDrawable(context, R.drawable.bg_chart)
            set1.fillDrawable = drawable

            set1.setDrawHorizontalHighlightIndicator(false)
            set1.fillFormatter =
                IFillFormatter { dataSet, dataProvider -> chart.getAxisLeft().getAxisMinimum() }
            set1.lineWidth = 3F

            // create a data object with the data sets
            val data = LineData(set1)


            data.setValueTypeface(tfLight)


            data.setValueTextSize(9f)


            //value of value
            data.setDrawValues(false)


            // set data
            chart.data = data

            var xMax: Int = 0

            for (i in values) {
                if (i.x > xMax) {
                    xMax = i.x.toInt()
                }
            }
//



        }
    }


}


class MyXAxisFormatter : IAxisValueFormatter {
    private val days = arrayOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 12, 11)

//    override fun getFormattedValue(value: Float, axis: AxisBase?): String {
//        if (days.getOrNull(value.toInt())!! < 5) {
//            axis?.textColor = (Color.RED)
//        } else if (days.getOrNull(value.toInt())!! > 5) {
//            axis?.textColor = (Color.BLUE)
//        }
//
//        axis?.mEntries
//        for (i in axis?.mEntries!!) {
////            Log.e("!!!i.toString()", i.toString())
////            Log.e("!!!ax", i.)
//        }
//
////        String().fo
//
////        Log.e("!!!ax", axis?.mEntries.toString())
//        return days.getOrNull(value.toInt()).toString() ?: value.toString()
////        return days.getOrNull(value.toInt()) ?: value.toString()
//    }


    override fun getFormattedValue(value: Float, axis: AxisBase?): String {

        return value.toString() ?: value.toString()

    }


}

class MyMarkerView(context: Context?, layoutResource: Int) :
    MarkerView(context, layoutResource) {
    private val tvContent: TextView = findViewById<View>(R.id.tvContent) as TextView

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    override fun refreshContent(
        e: Entry,
        highlight: Highlight
    ) {

//            tvContent.text = "" + Utils.formatNumber(
//                e.x,
//                0,
//                true
//            )

        tvContent.text = e.x.toString()

        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat())
    }

}
