package com.cashbro.shared

import android.content.Context
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import com.cashbro.data.HistoryItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class SharedPreferencesProvider(context: Context) {

    var masterKey = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
        .build()

    private val prefs =
        EncryptedSharedPreferences.create(
            context,
            "cashbro_android_app",
            masterKey,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )


    fun isFirstOpen() = prefs.getBoolean(IS_FIRST_OPEN, false)

    fun setFirstOpen(isFirstOpen: Boolean) {
        prefs.edit().putBoolean(IS_FIRST_OPEN, isFirstOpen).apply()
    }
    fun getUUID() = prefs.getString(UUID, "")

    fun setUUID(uuid: String) {

        prefs.edit().putString(UUID, uuid).apply()
    }

    fun getLocalHistory() = Gson().fromJson<ArrayList<String>>(prefs.getString(HISTORY, ""), object : TypeToken<ArrayList<String?>?>() {}.type)

    fun setLocalHistory(list: List<String>) {
        prefs.edit().putString(HISTORY, Gson().toJson(list)).apply()
    }

    fun getGlobalHistory() = Gson().fromJson<ArrayList<ArrayList<HistoryItem>>>(prefs.getString(HISTORY_GLOBAL, ""), object : TypeToken<ArrayList<ArrayList<HistoryItem>?>?>() {}.type)

    fun setGlobalHistory(list: ArrayList<ArrayList<HistoryItem>>) {
        prefs.edit().putString(HISTORY_GLOBAL, Gson().toJson(list)).apply()
    }


    companion object {
        private const val IS_FIRST_OPEN = "is_first_open"
        private const val UUID = "uuid"
        private const val HISTORY = "history"
        private const val HISTORY_GLOBAL = "history_global"
    }

}