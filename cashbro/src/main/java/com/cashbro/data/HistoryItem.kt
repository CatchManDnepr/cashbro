package com.cashbro.data

import android.graphics.Bitmap
import android.icu.text.CaseMap

data class HistoryItem(
    var url: ByteArray?,
    val title: String,
    val description: String
)
