package com.cashbro.ui

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.cashbro.R
import com.cashbro.data.HistoryItem
import com.cashbro.ui.serfing.SerfingFragment
import com.cashbro.utils.extensions.gone
import com.cashbro.utils.extensions.visible
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase

class MainLibActivity : AppCompatActivity() {

    var currentTab = 0

    var currentPage =-1

    var history: ArrayList<ArrayList<HistoryItem>> = ArrayList()

    var localHistory = ArrayList<Any>()

    var exit: LinearLayout? = null

    var iOnBackPressedListener: IOnBackPressedListener? = null

    var showOnBoardingInCurrent = false
    var collectDataInCurrent = false

    lateinit var database: DatabaseReference

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_lib)

        database = Firebase.database.reference

        exit = findViewById(R.id.llExit)

        exit?.setOnClickListener {
            finish()
        }
    }

    override fun onBackPressed() {

        if (iOnBackPressedListener == null || currentPage <= 1) {
            Log.e("!!!", "true")
            super.onBackPressed()
        } else {
            exit?.visible()
            Handler().postDelayed({
                                  exit?.gone()
            }, 2000)
            Log.e("!!!", "false")
            iOnBackPressedListener?.onBackPressed()
        }
    }
}