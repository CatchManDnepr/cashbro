package com.cashbro.ui.statistics

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cashbro.base.BaseKotlinFragment
import com.cashbro.databinding.FragmentStatisticBinding
import com.cashbro.ui.serfing.SerfingViewModel
import com.cashbro.utils.FORMAT_DATE_MONTH
import com.cashbro.utils.stub.StubNavigator
import com.github.mikephil.charting.data.Entry
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap





class StatisticFragment : BaseKotlinFragment() {

    override val viewModel: SerfingViewModel by viewModel()
    override val navigator: StatisticsNavigator = get()

    private lateinit var binding: FragmentStatisticBinding

    var countToday: Long = 0
    var countAll: Long = 0

    var uuid: String = ""

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStatisticBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        countAll = 0

        val data = Calendar.getInstance().time

        val sdf = SimpleDateFormat(FORMAT_DATE_MONTH)
        val s: String = sdf.format(data)


        var dataEntry = try {
            arguments?.getSerializable("hashmap") as ArrayList<Pair<String, Long>>
        } catch (e: Exception) {
            null
        }

        if(countAll == 0L && dataEntry != null) {
            for(v in dataEntry) {
                Log.e("!!!",v.toString())
                countAll += v.second

                if(v.first == s) {
                    countToday = v.second
                }
            }
        }


        binding.tvTotalEarned.text = "₽ " + String.format("%.3f", (countAll.toDouble() / 6000))
        binding.tvTodayEarned.text = "₽ " + String.format("%.3f", (countToday.toDouble() / 6000))

        val chartData = ArrayList<Entry>()

        chartData.add(Entry(0f, 0f, "Ранее"))

        dataEntry?.forEachIndexed { index, it ->
            chartData.add(Entry(index.toFloat() + 1, it.second.toFloat(), it.first))
        }


        binding.chartResonance.setData(chartData)


        binding.clBack.setOnClickListener {
            navController?.popBackStack()
        }

        binding.tvBackToBrowser.setOnClickListener {
            navController?.popBackStack()
        }
        binding.tvDecline.setOnClickListener {
            navigator.goToPolicy(navController)
        }

    }

 }