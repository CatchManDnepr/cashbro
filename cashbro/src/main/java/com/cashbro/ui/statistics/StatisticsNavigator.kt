package com.cashbro.ui.statistics

import android.os.Bundle
import androidx.navigation.NavController
import com.cashbro.R
import com.cashbro.base.BaseNavigator

class StatisticsNavigator: BaseNavigator() {

    fun goToAccept(navController: NavController?) {
        navController?.navigate(R.id.action_statisticFragment_to_acceptFragment)
    }

    fun goToPolicy(navController: NavController?) {
        navController?.navigate(R.id.action_statisticFragment_to_policyFragment)
    }

}