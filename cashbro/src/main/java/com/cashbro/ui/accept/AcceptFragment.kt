package com.cashbro.ui.accept

import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.cashbro.R
import com.cashbro.base.BaseKotlinFragment
import com.cashbro.databinding.FragmentAcceptBinding
import com.cashbro.ui.MainLibActivity
import com.cashbro.ui.accept.adapter.OnBoardingPagerAdapter
import com.cashbro.utils.extensions.bindDataTo
import com.cashbro.utils.extensions.gone
import com.cashbro.utils.extensions.visible
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel


class AcceptFragment : BaseKotlinFragment() {

    override val viewModel: AcceptViewModel by viewModel()
    override val navigator: AcceptNavigator = get()

    private lateinit var binding: FragmentAcceptBinding

    var dots: ArrayList<ImageView>? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAcceptBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        (activity as MainLibActivity).showOnBoardingInCurrent = true

        subscribeLiveData()

        binding.tvAccept.setOnClickListener {
            if (binding.vpOnBoarding.currentItem == 2) {
                isFirstOpen(true)
            } else {
                binding.vpOnBoarding.currentItem = binding.vpOnBoarding.currentItem + 1
            }
        }

        addDots()
//        binding.tvDecline.setOnClickListener {
//            activity?.finish()
//        }

        binding.tvDecline.visible()
        binding.tvAccept.text = "Дальше"

        binding.vpOnBoarding.adapter = OnBoardingPagerAdapter(parentFragmentManager)
        binding.vpOnBoarding.setOnScrollChangeListener { _, _, _, _, _ -> setButtonTitle() }

        binding.tvDecline.setOnClickListener {
            viewModel.setFirstOpen(true)
        }

//        binding.tabs.setupWithViewPager(binding.vpOnBoarding)
    }

    private fun setButtonTitle() {
        if (binding.vpOnBoarding.currentItem == 2) {
            binding.tvDecline.gone()
            binding.tvAccept.text = "Открыть браузер"
        } else {
            binding.tvDecline.visible()
            binding.tvAccept.text = "Дальше"
        }
    }

    private fun subscribeLiveData() {
        bindDataTo(viewModel.isFirstOpenLiveData, ::exit)
    }

    private fun isFirstOpen(isFirstOpen: Boolean) {
        if(isFirstOpen) {
            navigator.goToAddData(navController)
        } else {
            activity?.finish()
        }
    }

    private fun exit(isExit: Boolean) {
        if(isExit) {
            navigator.goToSerfing(navController)
        }
    }

    fun addDots() {
        dots = ArrayList()

        val scale = requireContext().resources.displayMetrics.density

        for (i in 0 until 3) {
            val dot = ImageView(requireContext())
            dot.setImageDrawable(resources.getDrawable(R.drawable.tab_indicator_default))
            val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.marginStart = ((4 * scale + 0.5f).toInt())
            params.topMargin = ((280 * scale + 0.5f).toInt())
            binding.dots.addView(dot, params)
            dots?.add(dot)
        }
        binding.vpOnBoarding.setOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                selectDot(position)
            }

            override fun onPageScrollStateChanged(state: Int) {}
        })

        selectDot(0)
    }

    fun selectDot(idx: Int) {
        val res: Resources = resources
        for (i in 0 until 3) {
            val drawableId: Int =
                if (i == idx) R.drawable.tab_indicator_selected else R.drawable.tab_indicator_default
            val drawable: Drawable = res.getDrawable(drawableId)
            dots?.get(i)?.setImageDrawable(drawable)
        }
    }


}