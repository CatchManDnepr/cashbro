package com.cashbro.ui.accept.adapter

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.cashbro.ui.pages.OnBoardingFirstPageFragment
import com.cashbro.ui.pages.OnBoardingSecondPageFragment
import com.cashbro.ui.pages.OnBoardingThirdPageFragment

class OnBoardingPagerAdapter(fragmentManager: FragmentManager) :
    FragmentStatePagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount() = 3

    override fun getItem(position: Int) =
        when (position) {
            0 -> OnBoardingFirstPageFragment.newInstance()
            1 -> OnBoardingSecondPageFragment.newInstance()
            else -> OnBoardingThirdPageFragment.newInstance()
        }

}