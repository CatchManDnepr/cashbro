package com.cashbro.ui.accept

import android.os.Bundle
import androidx.navigation.NavController
import com.cashbro.R
import com.cashbro.base.BaseNavigator

class AcceptNavigator: BaseNavigator() {

    fun goToAddData(navController: NavController?) {
        navController?.navigate(R.id.action_acceptFragment_to_addDataFragment)
    }

    fun goToSerfing(navController: NavController?) {
        navController?.navigate(R.id.action_acceptFragment_to_serfingFragment)
    }

}