package com.cashbro.ui.accept

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.cashbro.base.BaseViewModel
import com.cashbro.shared.SharedPreferencesProvider
import java.util.*

class AcceptViewModel(
    private val app: Application,
    private val sharedPreferencesProvider: SharedPreferencesProvider
): BaseViewModel(app) {

    val isFirstOpenLiveData = MutableLiveData<Boolean>()
    val goNext = MutableLiveData<String>()


    fun setFirstOpen(isFirstOpen: Boolean) {
        sharedPreferencesProvider.setFirstOpen(isFirstOpen)
        if(isFirstOpen) {
            isFirstOpenLiveData.postValue(isFirstOpen)
        } else  {
            isFirstOpenLiveData.postValue(isFirstOpen)
        }
    }

    fun getUUID() {
        var uuid = sharedPreferencesProvider.getUUID()

        if(uuid.isNullOrBlank()) {
            uuid = UUID.randomUUID().toString()
            sharedPreferencesProvider.setUUID(uuid)
        }

        goNext.postValue(uuid)
    }

}