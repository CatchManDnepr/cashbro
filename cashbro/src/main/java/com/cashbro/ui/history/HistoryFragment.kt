package com.cashbro.ui.history

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cashbro.R
import com.cashbro.base.BaseAdapter
import com.cashbro.base.BaseKotlinFragment
import com.cashbro.data.HistoryItem
import com.cashbro.databinding.FragmentHistoryBinding
import com.cashbro.databinding.FragmentPolicyBinding
import com.cashbro.databinding.FragmentStatisticBinding
import com.cashbro.ui.MainLibActivity
import com.cashbro.ui.history.adapter.HistoryHolder
import com.cashbro.ui.serfing.SerfingViewModel
import com.cashbro.utils.FORMAT_DATE_MONTH
import com.cashbro.utils.stub.StubNavigator
import com.cashbro.utils.stub.StubViewModel
import com.github.mikephil.charting.data.Entry
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap





class HistoryFragment : BaseKotlinFragment() {

    override val viewModel: StubViewModel by viewModel()
    override val navigator: StubNavigator = get()

    private lateinit var binding: FragmentHistoryBinding

    val adapter = BaseAdapter()
        .map(R.layout.item_history_data, HistoryHolder.TitleHolder())
        .map(R.layout.item_history_item, HistoryHolder.ItemHolder(::updateList))

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHistoryBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        binding.clBack.setOnClickListener {
            navController?.popBackStack()
        }

        val history = (activity as MainLibActivity).localHistory

        Log.e("!!!", history.toString())
        adapter.itemsLoaded(history)

        binding.rvHistory.adapter = adapter
    }

    fun updateList(historyItem: HistoryItem) {
        val history = (activity as MainLibActivity).localHistory
        history.remove(historyItem)
        adapter.itemsLoaded(history)
    }

 }