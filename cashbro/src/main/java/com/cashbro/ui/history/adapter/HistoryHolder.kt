package com.cashbro.ui.history.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.cashbro.R
import com.cashbro.base.Holder
import com.cashbro.data.HistoryItem

object HistoryHolder {

    class TitleHolder() : Holder<String>() {
        override fun bind(itemView: View, item: String) {
            val tvText = itemView.findViewById<TextView>(R.id.tvDate)
            tvText.text = item

        }
    }

    class ItemHolder(private val onItemClick: (HistoryItem) -> Unit) :
        Holder<HistoryItem>() {
        override fun bind(itemView: View, item: HistoryItem) {

            val ivIcon = itemView.findViewById<ImageView>(R.id.ivIcon)
            val ivClose = itemView.findViewById<ImageView>(R.id.ivClose)
            val tvText = itemView.findViewById<TextView>(R.id.tvTitle)
            val tvUrl = itemView.findViewById<TextView>(R.id.tvUrl)

            tvText.text = item.title
            tvUrl.text = item.description


            try {
                if(item.url != null) {

                    Glide
                        .with(itemView.context)
                        .load(item.url)
                        .centerCrop()
                        .into(ivIcon)
                }
            } catch (e: Exception) {

            }

            ivClose.setOnClickListener {
                onItemClick.invoke(item)
            }
        }
    }


}