package com.cashbro.ui.tabs.adapters

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.cashbro.R
import com.cashbro.base.Holder
import com.cashbro.data.HistoryItem

object TabsHolder {

    class ItemHolder(private val onItemClick: (HistoryItem) -> Unit, private val onCloseClick: (HistoryItem) -> Unit) :
        Holder<HistoryItem>() {
        override fun bind(itemView: View, item: HistoryItem) {

            val ivIcon = itemView.findViewById<ImageView>(R.id.ivImage)
            val ivClose = itemView.findViewById<ImageView>(R.id.ivClose)
            val tvUrl = itemView.findViewById<TextView>(R.id.tvUrl)

            tvUrl.text = item.description

            Glide
                .with(itemView.context)
                .load(item.url)
                .centerCrop()
                .into(ivIcon)

            ivClose.setOnClickListener {
                onCloseClick.invoke(item)
            }

            itemView.rootView.setOnClickListener {
                onItemClick.invoke(item)
            }
        }
    }


}