package com.cashbro.ui.tabs

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.cashbro.R
import com.cashbro.base.BaseAdapter
import com.cashbro.base.BaseKotlinFragment
import com.cashbro.data.HistoryItem
import com.cashbro.databinding.FragmentAddTabsBinding
import com.cashbro.databinding.FragmentHistoryBinding
import com.cashbro.databinding.FragmentPolicyBinding
import com.cashbro.databinding.FragmentStatisticBinding
import com.cashbro.ui.MainLibActivity
import com.cashbro.ui.history.adapter.HistoryHolder
import com.cashbro.ui.serfing.SerfingViewModel
import com.cashbro.ui.tabs.adapters.TabsHolder
import com.cashbro.utils.FORMAT_DATE_MONTH
import com.cashbro.utils.stub.StubNavigator
import com.cashbro.utils.stub.StubViewModel
import com.github.mikephil.charting.data.Entry
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class TabsFragment : BaseKotlinFragment() {

    override val viewModel: StubViewModel by viewModel()
    override val navigator: StubNavigator = get()

    private lateinit var binding: FragmentAddTabsBinding

    val adapter = BaseAdapter()
        .map(R.layout.item_tab, TabsHolder.ItemHolder({
            val history = (activity as MainLibActivity).history
            var data = history.map { it.last() }
            (activity as MainLibActivity).currentTab = data.indexOf(it)
            (activity as MainLibActivity).currentPage = try {
                history[data.indexOf(it)].size - 1
            } catch (e: Exception) {
                0
            }
            navController?.popBackStack()
        }, ::removeTab))

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddTabsBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        binding.clBack.setOnClickListener {
            navController?.popBackStack()
        }

        binding.tvAdd.setOnClickListener {
            val history = (activity as MainLibActivity).history
            history.add(ArrayList(listOf(HistoryItem(null, "Google", "https://google.com/"))))

            var data = history.map { it.last() }
            adapter.itemsLoaded(data)

            (activity as MainLibActivity).currentTab = data.size - 1
            navController?.popBackStack()
        }

        binding.ivAdd.setOnClickListener {
            val history = (activity as MainLibActivity).history
            history.add(ArrayList(listOf(HistoryItem(null, "Google", "https://google.com/"))))

            var data = history.map { it.last() }
            adapter.itemsLoaded(data)


            (activity as MainLibActivity).currentTab = data.size - 1

            navController?.popBackStack()
        }

        val history = (activity as MainLibActivity).history
        var data = history.map { it.last() }
        adapter.itemsLoaded(data)

        binding.rvTabs.layoutManager = GridLayoutManager(context, 2)
        binding.rvTabs.adapter = adapter
    }

    fun removeTab(historyItem: HistoryItem) {
        val history = (activity as MainLibActivity).history

        var data = ArrayList(history.map { it.last() })
        history.removeAt(data.indexOf(historyItem))
        data.remove(historyItem)
        adapter.itemsLoaded(data)
    }

 }