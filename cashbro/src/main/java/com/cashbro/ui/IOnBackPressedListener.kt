package com.cashbro.ui

interface IOnBackPressedListener {

    fun onBackPressed()

}