package com.cashbro.ui.pages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cashbro.R
import com.cashbro.base.BaseKotlinFragment
import com.cashbro.utils.stub.StubNavigator
import com.cashbro.utils.stub.StubViewModel
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel

class OnBoardingThirdPageFragment : BaseKotlinFragment() {

    override val viewModel: StubViewModel by viewModel()
    override val navigator: StubNavigator = get()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_onboarding_third, null)
    }


    companion object {
        fun newInstance() = OnBoardingThirdPageFragment()
    }

}