package com.cashbro.ui.serfing

import android.app.Application
import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.cashbro.base.BaseViewModel
import com.cashbro.data.HistoryItem
import com.cashbro.shared.SharedPreferencesProvider
import com.google.gson.Gson
import java.util.*
import kotlin.collections.ArrayList

class SerfingViewModel(
    private val app: Application,
    private val sharedPreferencesProvider: SharedPreferencesProvider
): BaseViewModel(app) {

    val isFirstOpenLiveData = MutableLiveData<Boolean>()
    val getUUIDLiveData = MutableLiveData<String>()
    val getLocalHistoryLiveData = MutableLiveData<ArrayList<String>>()
    val getGlobalHistoryLiveData = MutableLiveData<ArrayList<ArrayList<HistoryItem>>>()


    var dataEntry: ArrayList<Pair<String, Long>> = ArrayList()

    var countToday: Long = 0
    var countAll: Long = 0


    fun checkData() {
        isFirstOpenLiveData.postValue(sharedPreferencesProvider.isFirstOpen())
    }

    fun getUUID() {
        var uuid = sharedPreferencesProvider.getUUID()

        if(uuid.isNullOrBlank()) {
            uuid = UUID.randomUUID().toString()
            sharedPreferencesProvider.setUUID(uuid)
        }

        getUUIDLiveData.postValue(uuid ?: "")
    }

    fun saveLocalHistory(list: ArrayList<Any>) {
        sharedPreferencesProvider.setLocalHistory(list.map { Gson().toJson(it) })
    }

    fun getLocalHistory() {
        getLocalHistoryLiveData.postValue(sharedPreferencesProvider.getLocalHistory())
    }

    fun saveGlobalHistory(list: ArrayList<ArrayList<HistoryItem>>) {
        sharedPreferencesProvider.setGlobalHistory(list)
    }

    fun getGlobalHistory() {
        getGlobalHistoryLiveData.postValue(sharedPreferencesProvider.getGlobalHistory())
    }

}