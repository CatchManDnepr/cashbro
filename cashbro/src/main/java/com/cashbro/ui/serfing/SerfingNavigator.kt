package com.cashbro.ui.serfing

import android.os.Bundle
import androidx.navigation.NavController
import com.cashbro.R
import com.cashbro.base.BaseNavigator

class SerfingNavigator: BaseNavigator() {

    fun goToAccept(navController: NavController?) {
        navController?.navigate(R.id.action_serfingFragment_to_acceptFragment)
    }

    fun goToStatistics(navController: NavController?, bundle: Bundle) {
        navController?.navigate(R.id.action_serfingFragment_to_statisticFragment, bundle)
    }

    fun goToHistory(navController: NavController?) {
        navController?.navigate(R.id.action_serfingFragment_to_historyFragment)
    }

    fun goToChooseTab(navController: NavController?) {
        navController?.navigate(R.id.action_global_tabs_fragment)
    }

}