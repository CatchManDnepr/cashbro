package com.cashbro.ui.serfing

import android.graphics.Bitmap
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import androidx.core.os.bundleOf
import com.cashbro.base.BaseKotlinFragment
import com.cashbro.data.HistoryItem
import com.cashbro.databinding.FragmentSerfingBinding
import com.cashbro.ui.IOnBackPressedListener
import com.cashbro.ui.MainLibActivity
import com.cashbro.ui.dialogs.OptionseDialog
import com.cashbro.utils.FORMAT_DATE_MONTH
import com.cashbro.utils.extensions.bindDataTo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.ByteArrayOutputStream
import java.text.SimpleDateFormat
import java.util.*


class SerfingFragment : BaseKotlinFragment(), IOnBackPressedListener {

    override val viewModel: SerfingViewModel by viewModel()
    override val navigator: SerfingNavigator = get()

    private lateinit var binding: FragmentSerfingBinding

    var uuid: String = ""

    var loadFromHistory = false

    var isSerfing = false
    var isTimerRunning = false

    private val editorActionListener: TextView.OnEditorActionListener =
        TextView.OnEditorActionListener { v, actionId, event ->
            when (actionId) {
                EditorInfo.IME_ACTION_NEXT -> {

                }

                EditorInfo.IME_ACTION_SEARCH -> {

                    var url = binding.etUrl.text.toString()

                    url = url.replace("http://","").replace("https://","").replace("http:// www.","").replace("www.","")

                    if(Patterns.WEB_URL.matcher(url).matches()) {
                        binding.wvContent.loadUrl("https://$url")
                    } else {
//                    binding.wvContent.loadUrl("https://yandex.ru/search/?text=${binding.etUrl.text}")
                    binding.wvContent.loadUrl("https://www.google.com/search?q=${binding.etUrl.text}")
                    }
                }
            }
            false
        }

    private val countDown = object : CountDownTimer(9_223_372_036_854_775_807, 1000) {
        override fun onTick(millisUntilFinished: Long) {
            isTimerRunning = true
            if(isSerfing) {
                viewModel.countToday++
                updateData()
                showCountAll()
            }
        }

        override fun onFinish() {
            isTimerRunning = false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSerfingBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        if((activity as MainLibActivity).localHistory.isEmpty()) {
            viewModel.getLocalHistory()
        }

        if((activity as MainLibActivity).history.isEmpty()) {
            viewModel.getGlobalHistory()
            binding.ivTabs.text = "1"
        }

        (activity as MainLibActivity).iOnBackPressedListener = this

        if(!(activity as MainLibActivity).showOnBoardingInCurrent) {
            viewModel.checkData()
        }

        binding.ivTabs.setOnClickListener {
            try {
                navigator.goToChooseTab(navController)
            } catch (e: Exception) {

            }
        }

//        isFirstOpen(false)

        subscribeLiveData()

        //binding.wvContent.settings.userAgentString = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.31 (KHTML, like Gecko) Chrome/20 Safari/537.31"

//        binding.ivStatistic.setOnClickListener {
//            isSerfing = false
//            for (d in viewModel.dataEntry) {
//                Log.e("!!!hash", d.first)
//                Log.e("!!!max", viewModel.countAll.toString())
//                Log.e("!!!today", viewModel.countToday.toString())
//            }
//            navigator.goToStatistics(navController, bundleOf("hashmap" to viewModel.dataEntry))
//        }

        binding.ivOptions.setOnClickListener {
            val dialog = OptionseDialog.newInstance()
            dialog.initListener {
                when (it) {
                    OptionseDialog.Options.BACK -> {
                        loadFromHistory = true
                        val act = activity as MainLibActivity
                        Log.e("!!!", act.currentPage.toString())
                        act.currentPage = act.currentPage - 2
                        binding.wvContent.loadUrl(act.history[act.currentTab][act.currentPage].description)
                        dialog.dismiss()
                    }
                    OptionseDialog.Options.NEXT -> {
                        loadFromHistory = true
                        val act = activity as MainLibActivity
                        val history = act.history[act.currentTab]
                        if(history.size >= act.currentPage) {
                            act.currentPage++
                            binding.wvContent.loadUrl(act.history[act.currentTab][act.currentPage].description)
                        }
                        dialog.dismiss()
                    }
                    OptionseDialog.Options.REFRESH -> {
                        loadFromHistory = true
                        binding.wvContent.reload()
                        dialog.dismiss()
                    }
                    OptionseDialog.Options.HISTORY -> {
                        navigator.goToHistory(navController)
                        dialog.dismiss()
                    }
                    OptionseDialog.Options.STATISTICS -> {
                        isSerfing = false
                        for (d in viewModel.dataEntry) {
                            Log.e("!!!hash", d.first)
                            Log.e("!!!max", viewModel.countAll.toString())
                            Log.e("!!!today", viewModel.countToday.toString())
                        }
                        navigator.goToStatistics(navController, bundleOf("hashmap" to viewModel.dataEntry))
                        dialog.dismiss()
                    }
                }
            }
            dialog.show(childFragmentManager, OptionseDialog.TAG)
        }

        binding.etUrl.setOnEditorActionListener(editorActionListener)


        binding.ivHome.setOnClickListener {
//            binding.wvContent.loadUrl("https://yandex.ru/")
            binding.wvContent.loadUrl("https://google.com/")
        }

        isSerfing = true

        binding.wvContent.settings.javaScriptEnabled = true

        val webSettings: WebSettings = binding.wvContent.settings
        webSettings.javaScriptEnabled = true
        webSettings.useWideViewPort = true
        webSettings.loadWithOverviewMode = true

        try {
            val act = activity as MainLibActivity
            binding.wvContent.loadUrl(act.history[act.currentTab][act.currentPage].description)
        } catch (e: Exception) {
            binding.wvContent.loadUrl("https://google.com/")
        }

        binding.wvContent.webViewClient = object : WebViewClient() {

            override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
                super.onPageStarted(view, url, favicon)
                binding.etUrl.setText(url ?: "")
                Log.e("WebView", "your current url when webpage loading..$url")
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                val actvt = (activity as MainLibActivity)
                if(url?.isNotEmpty() == true) {
                    if(!loadFromHistory) {
                        actvt.currentPage++

                        var historyItem = HistoryItem(null, view?.title ?: "", url ?: "")

                        try {
                            actvt.history[actvt.currentTab].add(historyItem)
                        } catch (e: Exception) {
                            actvt.history.add(ArrayList())
                            actvt.history[actvt.currentTab].add(historyItem)
                        }

                        addImg(view, historyItem)

                        val data = Calendar.getInstance().time

                        val sdf = SimpleDateFormat(FORMAT_DATE_MONTH)
                        val s: String = sdf.format(data)

                        actvt.database
                            .child("Test app")
                            .child(uuid)
                            .child("history")
                            .child(
                                url
                                    .replace("/", "")
                                    .replace(".", "_")
                                    .replace("#", "")
                                    .replace("$", "")
                                    .replace("[", "")
                                    .replace("]", "")
                            )
                            .setValue(s)
                    } else {
                        loadFromHistory = false
                    }

                    viewModel.saveGlobalHistory(actvt.history)

                }
                Log.e("WebView", "your current url when webpage loading.. finish$url")
            }

            override fun onReceivedError(
                view: WebView,
                errorCode: Int,
                description: String,
                failingUrl: String
            ) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show()
            }
        }


        viewModel.getUUID()

    }

    private fun addImg(webView: WebView?, historyItem: HistoryItem){
        val handler = Handler()
        val runnable = Runnable {
            if(webView?.favicon != null) {

                val stream = ByteArrayOutputStream()
                webView.favicon!!.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val byteArray: ByteArray = stream.toByteArray()
                historyItem.url = byteArray

                (activity as MainLibActivity).history.map {
                    it.map { t ->
                        if(t.description == historyItem.description) {
                            t.url = byteArray
                        }
                        t
                    }
                }
            }

            val actvt = (activity as MainLibActivity)
            if(actvt.localHistory.isEmpty()) {
                val date = Calendar.getInstance().time

                val sdf = SimpleDateFormat(FORMAT_DATE_MONTH)
                val s: String = sdf.format(date)

                actvt.localHistory.add(0, s)

            } else {
                val data = actvt.localHistory.first() as? String ?: ""
                val date = Calendar.getInstance().time

                val sdf = SimpleDateFormat(FORMAT_DATE_MONTH)
                val s: String = sdf.format(date)

                if(data != s) {
                    actvt.localHistory.add(0, s)
                }
            }
            actvt.localHistory.add(1, historyItem)
            viewModel.saveLocalHistory(actvt.localHistory)
        }
        handler.postDelayed(runnable, 250) // delay time 200 ms
    }

    override fun onStart() {
        super.onStart()
        isSerfing = true

        val tabs =  (activity as MainLibActivity).history.size
        binding.ivTabs.text = if(tabs > 0) { tabs.toString() } else { "1" }
//        counter()
    }

    override fun onStop() {
        super.onStop()

        isSerfing = false

        (activity as MainLibActivity).iOnBackPressedListener = null
    }

    private fun subscribeLiveData() {
        bindDataTo(viewModel.isFirstOpenLiveData, ::isFirstOpen)
        bindDataTo(viewModel.getUUIDLiveData, ::initUUID)
        bindDataTo(viewModel.getLocalHistoryLiveData, ::initLocalHistory)
        bindDataTo(viewModel.getGlobalHistoryLiveData, ::initGlobalHistory)
    }

    override fun onBackPressed() {
        loadFromHistory = true
        val act = activity as MainLibActivity
        Log.e("!!!", act.currentPage.toString())
        act.currentPage = act.currentPage - 2
        binding.wvContent.loadUrl(act.history[act.currentTab][act.currentPage].description)
    }

    private fun isFirstOpen(isFirstOpen: Boolean) {
        if(!isFirstOpen) {
            try {
                navigator.goToAccept(navController)
            } catch (e: Exception) {

            }
        }
    }

    private fun initUUID(uuid: String) {
        this.uuid = uuid

        (activity as MainLibActivity).database.get().addOnSuccessListener {
            for (project in it.children) {
                if(project.key.toString() == "Test app") {
                    for (data in project.children) {

                        if(data.key == uuid){

                            val value = data.children

                            viewModel.countAll = 0
                            viewModel.dataEntry = ArrayList()
                            for(v in value) {
                                try {
                                    viewModel.dataEntry.add(v.key.toString() to v.value.toString().toLong())
                                    viewModel.countAll += v.value.toString().toLong()
                                    val d = Calendar.getInstance().time
                                    val sdf = SimpleDateFormat(FORMAT_DATE_MONTH)
                                    val s: String = sdf.format(d)
                                    if(s == v.key.toString()) {
                                        viewModel.countToday = v.value.toString().toLong()

                                    }
                                } catch (e: Exception) {

                                }


                            }

                        }
                    }
                }
            }

            if(!isTimerRunning) {
                counter()
            }
        }
    }

    private fun counter() {
        countDown.start()
    }

    override fun onPause() {
        super.onPause()
    }

    fun updateData() {
        val data = Calendar.getInstance().time

        val sdf = SimpleDateFormat(FORMAT_DATE_MONTH)
        val s: String = sdf.format(data)

        (activity as MainLibActivity).database.child("Test app").child(uuid).child(s).setValue(viewModel.countToday)

        initUUID(uuid)
    }

    fun showCountAll() {
        binding.tvEarned.text ="₽ " + String.format("%.4f", (viewModel.countAll.toDouble() / 2000))
    }

    fun initLocalHistory(list: ArrayList<String>) {

        val localList = ArrayList<Any>()

        list.forEach {
            if(it.startsWith("{")) {
                try {
                    localList.add(Gson().fromJson(it, object : TypeToken<HistoryItem?>() {}.type))
                } catch (e: Exception) {
                    try {
                        localList.add(Gson().fromJson(it, object : TypeToken<String?>() {}.type))
                    } catch (e: Exception) {

                    }
                }
            } else {
                localList.add(Gson().fromJson(it, object : TypeToken<String?>() {}.type))
            }
        }

        Log.e("!!!>>", localList.toString())

        if(list != null) {
                (activity as MainLibActivity).localHistory = localList
        }
    }

    fun initGlobalHistory(list: ArrayList<ArrayList<HistoryItem>>) {

        Log.e("!!!", list.toString())

        (activity as MainLibActivity).history = list

        binding.ivTabs.text = list.size.toString()

            binding.ivTabs.text = try {
            (list.size).toString()
        } catch (e: Exception) {
            "0"
        }
    }

}