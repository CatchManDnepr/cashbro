package com.cashbro.ui.dialogs

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cashbro.R
import com.cashbro.base.BaseKotlinDialogFragment
import com.cashbro.databinding.DialogOptionsBinding
import com.cashbro.utils.stub.StubViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class OptionseDialog() : BaseKotlinDialogFragment() {

    override val viewModel: StubViewModel by viewModel()

    var listener: ((Options) -> Unit)? = null

    private lateinit var binding: DialogOptionsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DialogOptionsBinding.inflate(inflater)
        return binding.root
    }

    override fun onResume() {
        super.onResume()

        dialog!!.window!!.setGravity(Gravity.RIGHT or Gravity.TOP)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.clBack.setOnClickListener {
            listener?.invoke(Options.BACK)
        }
        binding.clNext.setOnClickListener {
            listener?.invoke(Options.NEXT)
        }
        binding.clRefresh.setOnClickListener {
            listener?.invoke(Options.REFRESH)
        }
        binding.llHistory.setOnClickListener {
            listener?.invoke(Options.HISTORY)
        }
        binding.llStatistics.setOnClickListener {
            listener?.invoke(Options.STATISTICS)
        }
    }

    fun initListener(listener: ((Options) -> Unit)) {
        this.listener = listener
    }

    enum class Options {
        BACK, NEXT, REFRESH, HISTORY, STATISTICS
    }

    companion object {
        val TAG = OptionseDialog::class.java.name

        fun newInstance(): OptionseDialog {
            val fragment = OptionseDialog()
            fragment.setStyle(STYLE_NO_TITLE, R.style.DialogWhiteBG)

            return fragment
        }
    }
}