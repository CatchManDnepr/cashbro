package com.cashbro.ui.addData

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.cashbro.R
import com.cashbro.base.BaseKotlinFragment
import com.cashbro.databinding.FragmentAcceptBinding
import com.cashbro.databinding.FragmentAddDataBinding
import com.cashbro.ui.MainLibActivity
import com.cashbro.ui.accept.AcceptViewModel
import com.cashbro.ui.accept.adapter.OnBoardingPagerAdapter
import com.cashbro.utils.extensions.bindDataTo
import com.cashbro.utils.extensions.gone
import com.cashbro.utils.extensions.visible
import com.cashbro.utils.stub.StubNavigator
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel


class AddDataFragment : BaseKotlinFragment() {

    override val viewModel: AcceptViewModel by viewModel()
    override val navigator: AddDataNavigator = get()

    private lateinit var binding: FragmentAddDataBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAddDataBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        subscribeLiveData()

        binding.tvAccept.setOnClickListener {
            if(!binding.ivCheckbox.isSelected) {
                Toast.makeText(requireContext(), "Вы должны согласиться с политикой конфидкнциальности", Toast.LENGTH_SHORT).show()
            } else if(binding.tvPhone.text.isNullOrBlank()) {
                Toast.makeText(requireContext(), "Введите номер телефона", Toast.LENGTH_SHORT).show()
            } else if(binding.tvName.text.isNullOrBlank()) {
                Toast.makeText(requireContext(), "Введите Ваше имя", Toast.LENGTH_SHORT).show()

            } else {
                viewModel.getUUID()
            }
        }

        binding.tvDecline.setOnClickListener {
            navController?.popBackStack(R.id.main_nav, false)
        }

        binding.ivCheckbox.setOnClickListener {
            it.isSelected = !it.isSelected
        }

        binding.tvPolicy.setOnClickListener {
            navigator.goToPolicy(navController)
        }

        binding.tvPolicy.text = Html.fromHtml("Я согласен с <u><font color=#089379>Политикой конфиденциальности</font></u><br><br>")
    }

    private fun subscribeLiveData() {
        bindDataTo(viewModel.goNext, ::sendData)
        bindDataTo(viewModel.isFirstOpenLiveData, ::isFirstOpen)
    }

    private fun sendData(uuid: String) {
        (activity as MainLibActivity).database.child("Test app").child(uuid).child("userData").child("name").setValue(binding.tvName.text.toString())
        (activity as MainLibActivity).database.child("Test app").child(uuid).child("userData").child("phone").setValue(binding.tvPhone.text.toString())

        viewModel.setFirstOpen(true)
    }

    private fun isFirstOpen(isFirstOpen: Boolean) {
        navController?.popBackStack(R.id.main_nav, false)
    }


}