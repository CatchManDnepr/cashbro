package com.cashbro.ui.addData

import android.os.Bundle
import androidx.navigation.NavController
import com.cashbro.R
import com.cashbro.base.BaseNavigator

class AddDataNavigator: BaseNavigator() {

    fun goToPolicy(navController: NavController?) {
        navController?.navigate(R.id.action_addDataFragment_to_policyFragment)
    }

}