package com.cashbro.ui.policy

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cashbro.base.BaseKotlinFragment
import com.cashbro.databinding.FragmentPolicyBinding
import com.cashbro.databinding.FragmentStatisticBinding
import com.cashbro.ui.serfing.SerfingViewModel
import com.cashbro.utils.FORMAT_DATE_MONTH
import com.cashbro.utils.stub.StubNavigator
import com.cashbro.utils.stub.StubViewModel
import com.github.mikephil.charting.data.Entry
import org.koin.android.ext.android.get
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap





class PolicyFragment : BaseKotlinFragment() {

    override val viewModel: StubViewModel by viewModel()
    override val navigator: StubNavigator = get()

    private lateinit var binding: FragmentPolicyBinding


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPolicyBinding.inflate(inflater)
        return binding.root
    }

    override fun onStart() {
        super.onStart()

        binding.clBack.setOnClickListener {
            navController?.popBackStack()
        }

    }

 }