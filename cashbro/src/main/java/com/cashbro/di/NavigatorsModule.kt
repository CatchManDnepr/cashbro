package com.cashbro.di

import com.cashbro.ui.accept.AcceptNavigator
import com.cashbro.ui.addData.AddDataNavigator
import com.cashbro.ui.serfing.SerfingNavigator
import com.cashbro.ui.statistics.StatisticsNavigator
import com.cashbro.utils.stub.StubNavigator
import org.koin.dsl.module


val navigatorsModule = module {

    factory { StubNavigator() }
    factory { SerfingNavigator() }
    factory { StatisticsNavigator() }
    factory { AcceptNavigator() }
    factory { AddDataNavigator() }

}