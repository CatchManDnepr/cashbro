package com.cashbro.di

import com.cashbro.ui.accept.AcceptViewModel
import com.cashbro.ui.serfing.SerfingViewModel
import com.cashbro.utils.stub.StubViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {

    viewModel { StubViewModel(get()) }
    viewModel { SerfingViewModel(get(), get()) }
    viewModel { AcceptViewModel(get(), get()) }

}