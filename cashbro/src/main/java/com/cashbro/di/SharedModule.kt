package com.cashbro.di

import com.cashbro.shared.SharedPreferencesProvider
import org.koin.dsl.module

val sharedModule = module(override = true) {

    single { SharedPreferencesProvider(get()) }

}