package com.cashbro

import android.app.Application

class App : Application() {


    override fun onCreate() {
        super.onCreate()
        KoinProvider.startKoin(this)


    }


}