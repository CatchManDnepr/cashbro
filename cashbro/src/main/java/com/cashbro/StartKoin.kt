package com.cashbro

import com.cashbro.App
import com.cashbro.di.*
import org.koin.android.ext.koin.androidContext


object KoinProvider {

    @JvmStatic
    fun startKoin(app: App) {
        org.koin.core.context.startKoin {
            androidContext(app)
            modules(listOf(appModule, viewModelModule, navigatorsModule, sharedModule, repositoryModule))
        }
    }
}
