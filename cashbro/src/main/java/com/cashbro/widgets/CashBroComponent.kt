package com.cashbro.widgets

import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import com.cashbro.ui.MainLibActivity
import com.cashbro.R

class CashBroComponent @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr) {


    private lateinit var view: View

    init {
        initView()
    }

    private fun initView() {
        view = inflate(context, R.layout.cash_bro_component, this)
        initData()
    }

    private fun initData() {
        setOnClickListener {
            it.context.startActivity(Intent(it.context, MainLibActivity::class.java))
        }
    }
}