package com.cashbro.base

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.cashbro.utils.extensions.bindDataTo

abstract class BaseKotlinDialogFragment : DialogFragment() {

//    abstract val layoutRes: Int

    abstract val viewModel: BaseViewModel

//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? =
//        inflater.inflate(layoutRes, container, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindDataTo(viewModel.onStartProgress, ::startProgress)
        bindDataTo(viewModel.onEndProgress, ::endProgress)
        viewModel.showError.observe(this, ::showError)
        onReceiveParams(arguments)
    }

    private fun showError(s: String?) {
        if(s == null) return

        //ErrorDialog.newInstance(s).show(parentFragmentManager, ErrorDialog.TAG)
    }

    private fun startProgress(unit: Unit?) {
        //dialogProgressBar.show(parentFragmentManager, ProgressBarDialog.TAG)
    }

    private fun endProgress(unit: Unit?) {
        //dialogProgressBar.dismiss()
    }

    protected open fun onReceiveParams(arguments: Bundle?) {}

}